describe('Calculator', function() {
	describe("#sum", function(){
		it('returns sum of two values', function(){
			var calc = new Calculator(), result;

			result = calc.sum(1,2);
			result.should.equal(3);


		});
	});

	describe("#div", function(){
		it('returns result of division', function(){
			var calc = new Calculator(), result;

			result = calc.div(6,2);
			result.should.equal(3);
		});
	});

	describe("#div", function(){
		it('returns exception if divisor is zero', function(){
			var calc = new Calculator(), result;
			(function(){calc.div(6,0);}).should.throw;
		});
	});
});