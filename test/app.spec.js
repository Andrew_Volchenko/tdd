describe('app', function() {

	describe("#setTargetCapacity", function(){
		it('returns "not a number value" exception', function(){
			var app = new App(), result;
			(function(){ app.setTargetCapacity("awd")}).should.throw;

		});
	});

	describe("#setMaxCapacityOfTub", function(){
		it('returns "not a number value" exception', function(){
			var app = new App(), result;
			(function(){ app.setMaxCapacityOfTub(app.Tub1, "awd")}).should.throw;

		});
	});

	describe("#fill", function(){
		it('returns capacity of filling tub', function(){
			
			var app = new App(), result;
			app.setMaxCapacityOfTub(app.Tub1, 3);
			result = app.fillTub(app.Tub1);
			result.should.equal(3);

		});

		it('throws "Tub is full" exception', function(){
			var app = new App();

			app.setMaxCapacityOfTub(app.Tub1, 3);

			(function(){ app.fillTub(app.Tub1); app.fillTub(app.Tub1) }).should.throw;
		});

		it('throws "Can not fill tub with capacity = 0" exception', function(){
			var app = new App();

			(function(){ app.fillTub(app.Tub1); }).should.throw;
		});

	});

	describe("#pourOut", function(){
		it('returns 0 (0 is capacity of empty tub)', function(){
			
			var app = new App(), result;

			app.setMaxCapacityOfTub(app.Tub1, 3);
			app.setMaxCapacityOfTub(app.Tub2, 4);

			app.fillTub(app.Tub1);
			result = app.pourOut(app.Tub1)
			result.should.equal(0);

		});

		it('throws tub is empty exception', function(){
			
			var app = new App();

			(function(){ app.pourOut(app.Tub1) }).should.throw;
		});
	});

	describe("#pourFromTo", function(){
		it('returns currentCapacity of src tub', function(){
			
			var app = new App(), result;
			app.setMaxCapacityOfTub(app.Tub1, 3);
			app.setMaxCapacityOfTub(app.Tub2, 4);
			app.fillTub(app.Tub1);
			app.pourFromTo(app.Tub1, app.Tub2);
			app.fillTub(app.Tub1);
			result = app.pourFromTo(app.Tub1, app.Tub2);
			result.should.equal(2);

		});

		it('throws target tub is full exception', function(){
			
			var app = new App(), result;
			app.setMaxCapacityOfTub(app.Tub1, 3);
			app.setMaxCapacityOfTub(app.Tub2, 4);
			app.fillTub(app.Tub1);
			app.fillTub(app.Tub2);
			(function(){ app.pourFromTo(app.Tub2, app.Tub1) }).should.throw;
		});
	});

	describe("#win", function(){
		it('returns true (win)', function(){
			var app = new App(), result;
			debugger;
			app.setMaxCapacityOfTub(app.Tub1, 3);
			app.setMaxCapacityOfTub(app.Tub2, 4);
			app.fillTub(app.Tub1);
			app.pourFromTo(app.Tub1, app.Tub2);
			app.fillTub(app.Tub1);
			app.pourFromTo(app.Tub1, app.Tub2);
			app.pourOut(app.Tub2);
			result = app.checkWin();
			result.should.equal(true);
		});

		it('returns false (not win yet)', function(){
			var app = new App(), result;
			app.setMaxCapacityOfTub(app.Tub1, 3);
			app.setMaxCapacityOfTub(app.Tub2, 4);
			app.fillTub(app.Tub1);
			result = app.checkWin();
			result.should.equal(false);
		});
	});


});