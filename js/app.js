var App = function(){

		this.targetCapacity = 2;

		this.Tub1 = {capacity:0, maxCapacity: 0};
		this.Tub2 = {capacity:0, maxCapacity: 0};

		this.setTargetCapacity = function(val){
			if(+val != val){
				throw new Error('not a number value');
			} 

			this.targetCapacity = val;
			return val;
		},

		this.setMaxCapacityOfTub = function(target, val){
			
			if(+val != val){
				throw new Error('not a number value');
			}

			target.maxCapacity = val;
			return val;
			
		}

		this.fillTub = function(tub){
			
			if(tub.capacity == tub.maxCapacity){
				throw new Error('Tub is full');
			}
			
			tub.capacity = tub.maxCapacity;
			this.checkWin()
			return tub.capacity;
		},

		this.pourOut = function(tub){
			if(tub.capacity == 0){
				throw new Error('Tub is empty');
			}

			tub.capacity = 0;
			this.checkWin()
			return 0;

		},

		this.pourFromTo = function(src, dest){
			if(dest.capacity == dest.maxCapacity){
				throw new Error('target tub is full');
			}

			var destDif = dest.maxCapacity - dest.capacity; 
				
			if(src.capacity > destDif ){
				dest.capacity = dest.maxCapacity;
				src.capacity -= destDif;
			} else {
				dest.capacity += src.capacity;
				src.capacity = 0;
			}

			this.checkWin()
			return src.capacity;
		},

		this.checkWin = function(){
			if(this.Tub1.capacity + this.Tub2.capacity == this.targetCapacity){
				return true;
			}
			return false;
		}

		return this;
	};